-- 1. Customer is Midland Funding LLC
-- 2. Jobs created in 2021
-- 3. Jobs with a court state of Georgia
-- 4. Export of context "FILING" and a channel of "filed"


select j.id,j.created, c.name,co.name,e.export_channel,e.export_context,j.*
from jobs j
join customer_cases cc on j.customer_case_id = cc.id
join customers c on cc.customer_id = c.id
join job_civil_lawsuits jcl on jcl.job_id = j.id
join courts co on co.id = jcl.court_id
join exports e on e.job_id = j.id
join states s on co.jurisdiction_state_id=s.id
where c.name like 'Midland%'
and extract(year from j.created)=2021
and co.name like 'GA%'
and s.name like 'Georgia'
and e.export_channel = 'filed'
and e.export_context= 'FILING'
limit 10




/*
SELECT information_schema.columns.column_name, information_schema.columns.table_name
FROM information_schema.columns
WHERE information_schema.columns.column_name ILIKE '%job_address_id%'
ORDER BY 1
*/

/*
select  export_context from exports
limit 10
*/

select count(linked_job_address_id)
from investigation_results_with_vendor_data
group by servee_id
limit 1000

select *
from job_addresses
limit 500
